# common-lisp

Procedural, functional, object-oriented, meta, reflective, generic.

![Packages providing lisp-compiler](https://qa.debian.org/cgi-bin/popcon-png?packages=sbcl+ecl+maxima-sage+clisp+racket+gcl+cmucl+abcl+picolisp+newlisp&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%Y)

# Wiki
* [Common_Lisp](https://en.m.wikipedia.org/wiki/Common_Lisp) (WikipediA)
* [CommonLisp](https://wiki.debian.org/CommonLisp) (Debian)

# Books
* [*Let Over Lambda—50 Years of Lisp*
  ](https://www.worldcat.org/search?q=ti%3ALet+Over+Lambda%E2%80%9450+Years+of+Lisp)
  2008 Doug Hoyte
  * [letoverlambda.com](https://letoverlambda.com)

# Libraries
## Common Lisp Object System
* Inspired by Smalltalk (find a reference)
* [*Common Lisp Object System*](https://en.m.wikipedia.org/wiki/Common_Lisp_Object_System)
